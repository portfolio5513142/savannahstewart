let buttons = document.getElementsByClassName('jumpButton');
for (let i = 0; i < buttons.length; i++) {
    buttons[i].addEventListener('click', function(e) {
        e.preventDefault();
        let targetElement = document.getElementById(e.target.parentElement.dataset.target);
        window.scrollTo({ top: (targetElement.offsetTop - 160), left: 0, behavior: 'smooth'});
    });
}
